<?php
	/**
	 * $Id$
	 * A cloudy day Mobile
	 * Theme by Carette Donny
	 */
?>
<?php print "<?xml version='1.0' encoding='UTF-8' ?>"; ?>
<!DOCTYPE html PUBLIC "-//OPENWAVE//DTD XHTML Mobile 1.0//EN"
"http://www.openwave.com/dtd/xhtml-mobile10.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.5; user-scalable=1;"/>
  <?php print $styles; ?>
  <link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
  <?php print $scripts; ?>  
</head>
<body>      
  <div id="top">
    <div id="topmenu">
      <?php if (isset($primary_links)) : ?>
        <?php print theme('links', $primary_links, array('class' => 'menu')) ?>
      <?php endif; ?>
    </div>
  </div>
  <?php if (!empty($secondary_links)) : ?>
    <div id="submenu">
      <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
    </div>
  <?php endif; ?>  
  <div id="header">
    <div id="headertitle">
      <h1><a href="<?php print $front_page;?>" title="<?php print t('Home') ?>"><?php print $site_name;?></a></h1>
      <div class='site-slogan'>
        <?php print $site_slogan ;?>
      </div>
    </div>
    <?php if ($logo) : ?>
      <div id="logo">
        <a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print($logo) ?>" alt="<?php print t('Home') ?>" /></a>
      </div>
    <?php endif; ?>
    <?php if ($search_box): ?><div class="block block-theme"><?php print $search_box ?></div><?php endif; ?>
  </div><!-- /header -->
  <?php if ($show_messages && $messages): print "<div id=\"messagebox\">".$messages."</div>"; endif; ?>        
  <div id="contentcontainer">
    <div id="container">
      <div id="contentleft">
        <div id="page">
          <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php print $help; ?>
          <?php print $content ?>
          <div class="feedicons">
            <?php print $feed_icons ?>
          </div>
        </div>
        <?php if ($footer): print "<div id=\"footer\">".$footer."</div>"; endif;?>
      </div><!-- /contentleft -->
    </div><!-- /container -->
  </div><!-- /contentcontainer -->
  <div id="bottompage">
    <div id="skyline"></div>
    <div id="bottomtext">
      Theme designed by <a href="http://www.carettedonny.be" title="Donny Carette">Donny Carette</a> - copyright &copy; <?php print date("Y");?>
    </div>
  </div>    
  <?php print $closure ?>
</body>
</html>